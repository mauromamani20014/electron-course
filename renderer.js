// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.

const { remote } = require('electron');
const { dialog } = remote;

// const button = document.getElementById('btn-test');

button.addEventListener('click', () => {
  dialog.showMessageBox({ message: 'Dialog invoked from renderer process' });
  // podemos obtener la instancia del window
  const currentWin = remote.getCurrentWindow();
  currentWin.maximize();
});

setTimeout(() => {
  const notification = new Notification('Electron app', {
    body: 'Some info',
  });
}, 3000);
