// COOKIES

// Modules
const { app, BrowserWindow, session } = require('electron');

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;

// Create a new BrowserWindow when `app` is ready
function createWindow() {
  const ses = session.defaultSession;

  const getCookies = () => {
    ses.cookies.get({}).then(console.log).catch(console.err);
  };

  mainWindow = new BrowserWindow({
    width: 1000,
    height: 800,
    webPreferences: {
      // --- !! IMPORTANT !! ---
      // Disable 'contextIsolation' to allow 'nodeIntegration'
      // 'contextIsolation' defaults to "true" as from Electron v12
      contextIsolation: false,
      nodeIntegration: true,
    },
  });

  // Load index.html into the new BrowserWindow
  mainWindow.loadFile('session.html');

  // CREATE COOKIE
  // const cookie = {
  //   url: 'http://myapp.com',
  //   name: 'cookie1',
  //   value: 'electron',
  // };

  // REMOVE COOKIE
  // ses.cookies
  //   .remove('http://myapp.com', 'cookie1')
  //   .then(getCookies)
  //   .catch(console.error);

  // SET COOKIES
  // ses.cookies
  //   .set(cookie)
  //   .then(() => console.log('Cookie setted'))
  //   .catch(console.error);

  // SES DOWNLOAD API
  ses.on('will-download', (e, downloadItem, webContents) => {
    e.preventDefault();
    console.log('starting download: ');
    console.log(downloadItem.getFilename());
    console.log(downloadItem.getTotalBytes());
  });

  // Open DevTools - Remove for PRODUCTION!
  mainWindow.webContents.openDevTools();

  // Show cookies
  // mainWindow.webContents.on('did-finish-load', () => {
  //   getCookies();
  // });

  // Listen for window being closed
  mainWindow.on('closed', () => {
    mainWindow = null;
  });
}

// Electron `app` is ready
app.on('ready', createWindow);

// Quit when all windows are closed - (Not macOS - Darwin)
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') app.quit();
});

// When app icon is clicked and app is running, (macOS) recreate the BrowserWindow
app.on('activate', () => {
  if (mainWindow === null) createWindow();
});
