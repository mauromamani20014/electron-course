// Modules const { app, BrowserWindow } = require('electron');

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow, secondaryWindow;

// Create a new BrowserWindow when `app` is ready
function createWindow() {
  mainWindow = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      // --- !! IMPORTANT !! ---
      // Disable 'contextIsolation' to allow 'nodeIntegration'
      // 'contextIsolation' defaults to "true" as from Electron v12
      contextIsolation: false,
      nodeIntegration: true,
    },
    // show: false, para el evento ready-to-show
  });

  // Tambien puede tener los eventos como on.focus, etc
  secondaryWindow = new BrowserWindow({
    width: 400,
    height: 300,
    parent: mainWindow,
    webPreferences: {
      contextIsolation: false,
      nodeIntegration: true,
    },
    // show: false,
  });

  // Load index.html into the new BrowserWindow
  // Pareciera ser mejor que utilizat el loadURL, se carga mas rapido con loadfile
  mainWindow.loadFile('index.html');
  secondaryWindow.loadFile('secondaryWindow.html');

  // Open DevTools - Remove for PRODUCTION!
  // mainWindow.webContents.openDevTools();

  // Evitar el flash blanco cuando iniciamos la app
  // podemos evitar usar este evento, cambiano el color del background de las opciones del BrowserWindow instance
  // mainWindow.once('ready-to-show', mainWindow.show);

  // Listen for window being closed
  mainWindow.on('closed', () => {
    mainWindow = null;
  });

  secondaryWindow.on('closed', () => {
    secondaryWindow = null;
  });
}

// APP METHODS
// app.quit(): cerrar todas las pestañas

/*
// APP EVENTS

// before-quit: Lifecycle hook
app.on('before-quit', (ev) => {
  console.log('before-quit hook');
  // some code before to close
  // CODE
  // app.quit()

  // esto previene la ejecucion de ctrl+q, en windows este evento no es emitido
  ev.preventDefault();
});

// browser-window-blur
app.on('browser-window-blur', () => {
  console.log('browser-window-blur: App unfocused');
});

//  browser-window-focus
app.on('browser-window-focus', () => {
  console.log('browser-window-focus: App focused');
});

*/

// Electron `app` is ready
app.on('ready', () => {
  console.log(app.getPath('desktop'));
  console.log(app.getPath('music'));
  console.log(app.getPath('temp'));
  console.log(app.getPath('userData')); // ideal para guardar datos del programa, se usa para mantener una consistencia de donte estan los datos
  createWindow();
});

// Quit when all windows are closed - (Not macOS - Darwin)
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') app.quit();
});

// When app icon is clicked and app is running, (macOS) recreate the BrowserWindow
app.on('activate', () => {
  if (mainWindow === null) createWindow();
});
