// MENUS
// Modules
const { app, BrowserWindow, Menu, MenuItem } = require('electron');

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;

// Create a new BrowserWindow when `app` is ready
function createWindow() {
  // CREATE MENUS
  const mainMenu = new Menu();
  const item1 = new MenuItem({
    label: 'electron',
    submenu: [
      {
        label: 'Click me',
        click: () => {
          console.log('you bitch!');
        },
        enabled: true,
        // accelerator: 'CommandOrControl+g',
      },
      {
        label: 'Item 2',
        submenu: [{ label: 'sub 1' }, { label: 'sub 2' }],
      },
      {
        label: 'ToggleFullScreen',
        role: 'togglefullscreen',
      },
    ],
  });
  mainMenu.append(item1);

  mainWindow = new BrowserWindow({
    width: 1000,
    height: 800,
    webPreferences: {
      // --- !! IMPORTANT !! ---
      // Disable 'contextIsolation' to allow 'nodeIntegration'
      // 'contextIsolation' defaults to "true" as from Electron v12
      contextIsolation: false,
      nodeIntegration: true,
    },
  });

  // Load index.html into the new BrowserWindow
  mainWindow.loadFile('index.html');

  // Open DevTools - Remove for PRODUCTION!
  mainWindow.webContents.openDevTools();

  // CARGAR LOS MENUS
  Menu.setApplicationMenu(mainMenu);

  mainWindow.webContents.on('did-finish-load', () => {});

  mainWindow.on('closed', () => {
    mainWindow = null;
  });
}

// Electron `app` is ready
app.on('ready', createWindow);

// Quit when all windows are closed - (Not macOS - Darwin)
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') app.quit();
});

// When app icon is clicked and app is running, (macOS) recreate the BrowserWindow
app.on('activate', () => {
  if (mainWindow === null) createWindow();
});
